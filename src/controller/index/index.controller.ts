import { Controller, Get, Render } from '@nestjs/common';

@Controller()
export class IndexController {
    @Get()
    @Render('index')
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    public async index(): Promise<void> {}
}
