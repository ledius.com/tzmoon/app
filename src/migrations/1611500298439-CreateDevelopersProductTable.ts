import { MigrationInterface, QueryRunner, Table } from "typeorm";

/**
 * @migration
 * @class
 */
export class CreateDevelopersProductTable1611500298439 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'developers_product',
            columns: [
                {
                    name: 'id',
                    type: 'uuid',
                    isPrimary: true,
                    isGenerated: true,
                    generationStrategy: 'uuid'
                },
                {
                    name: 'avatar',
                    type: 'character',
                    length: '255',
                },
                {
                    name: 'name',
                    type: 'character',
                    length: '255',
                },
                {
                    name: 'price',
                    type: 'numeric',
                    precision: 10,
                    scale: 2,
                },
                {
                    name: 'performance',
                    type: 'int',
                },
            ]
        }))
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable("developers_product");
    }

}
