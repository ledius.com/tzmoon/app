import { MigrationInterface, QueryRunner, Table, TableForeignKey } from "typeorm";

/**
 * @migration
 * @class
 */
export class CreateWalletTable1611352047720 implements MigrationInterface {

    /**
     *
     * @param queryRunner
     */
    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(new Table({
            name: 'wallets',
            columns: [
                {
                    name: 'id',
                    isGenerated: true,
                    isPrimary: true,
                    type: 'uuid',
                    generationStrategy: 'uuid'
                },
                {
                    name: 'balance',
                    type: 'numeric',
                    precision:  10,
                    scale: 2,
                    default: 0,
                },
                {
                    name: 'users_id',
                    type: 'uuid',
                }
            ]
        }));
        await queryRunner.createForeignKey("wallets", new TableForeignKey({
            columnNames: ["users_id"],
            referencedTableName: "users",
            referencedColumnNames: ["id"],
            onDelete: "CASCADE",
            onUpdate: "CASCADE"
        }));
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        const table = await queryRunner.getTable("wallets");
        const fk = table.foreignKeys.find(fk => fk.columnNames.indexOf("users_id") !== -1);
        await queryRunner.dropForeignKey("wallets", fk);
        await queryRunner.dropTable(table);
    }
}
