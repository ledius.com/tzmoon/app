import { Body, Controller, Post } from '@nestjs/common';
import { ApiCreatedResponse, ApiTags } from "@nestjs/swagger";
import { SignupService } from "@app/signup";
import { SignupUserModel } from "@app/signup/model/signup-user-model";
import { SignupUserDto } from "@app/signup/dto/signup-user-dto";

@Controller('signup')
@ApiTags("Signup")
export class SignupController {
    constructor(
        private readonly signupService: SignupService
    ) {}

    @Post("/user")
    @ApiCreatedResponse({
        type: SignupUserModel,
    })
    public async signup(@Body() dto: SignupUserDto): Promise<SignupUserModel> {
        return await this.signupService.signup(dto);
    }
}
