import { PasswordsRepository } from './passwords-repository';
import { Connection } from "typeorm";

describe('PasswordsRepository', () => {
  it('should be defined', () => {
    const mockConnection = {
      getRepository: jest.fn()
    } as unknown as Connection;
    expect(new PasswordsRepository(mockConnection)).toBeDefined();
  });
});
