import { Injectable } from "@nestjs/common";
import { Connection, Repository } from "typeorm";
import { Password, PasswordsRepositoryInterface } from "@app/passwords";

@Injectable()
export class PasswordsRepository implements PasswordsRepositoryInterface {

    private readonly repository: Repository<Password> = this.connection.getRepository(Password);

    constructor(
        private readonly connection: Connection
    ) {}

    public async findByUserId(userId: string): Promise<Password[]> {
        return await this.repository.find({
            where: {
                user: {
                    id: userId
                }
            }
        });
    }

    public async save(password: Password): Promise<void> {
        await this.repository.save(password);
    }

    public async update(password: Password): Promise<void> {
        await this.repository.save(password);
    }


}
