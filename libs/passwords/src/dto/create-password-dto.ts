import { ApiProperty } from "@nestjs/swagger";
import { IsOptional, IsString } from "class-validator";

export class CreatePasswordDto {
    @ApiProperty({
        required: false
    })
    @IsString()
    @IsOptional()
    id?: string;

    @ApiProperty()
    @IsString()
    value: string;

    @ApiProperty()
    @IsString()
    user: string;
}
