import { Injectable } from '@nestjs/common';
import {
    InjectPasswordsRepository,
    PasswordsRepositoryInterface
} from "@app/passwords/repository/passwords-repository.interface";
import { Password } from "@app/passwords/entity/password";
import { CreatePasswordDto } from "@app/passwords/dto/create-password-dto";
import { UsersService } from "@app/users";
import * as uuid from "uuid";

@Injectable()
export class PasswordsService {
    constructor(
        @InjectPasswordsRepository() private readonly passwords: PasswordsRepositoryInterface,
        private readonly users: UsersService,
    ) {}

    public async create(dto: CreatePasswordDto): Promise<Password> {
        const user = await this.users.getById(dto.user);
        const id = dto.id || uuid.v4();
        const password = new Password(id, dto.value, user);
        await this.passwords.save(password);
        return password;
    }

    public async findByUserId(userId: string): Promise<Password[]> {
        return this.passwords.findByUserId(userId);
    }
}
