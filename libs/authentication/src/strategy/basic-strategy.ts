import { PassportStrategy } from "@nestjs/passport";
import { BasicStrategy as Strategy } from "passport-http";
import { Injectable, UnauthorizedException } from "@nestjs/common";
import { AuthenticationService } from "@app/authentication/service/authentication.service";
import { User } from "@app/users";

@Injectable()
export class BasicStrategy extends PassportStrategy(Strategy) {
    constructor(
        private readonly authentication: AuthenticationService,
    ) {
        super();
    }

    public async validate(username: string, password: string): Promise<User> {
        const user = await this.authentication.validateUser(username, password);
        if(!user) {
            throw new UnauthorizedException();
        }
        return user;
    }
}
