export class UserLoginAlreadyExistsException extends Error {
    public readonly login: string;
    constructor(login: string) {
        super(`Login ${login} already exists`);
        this.login = login;
    }
}
