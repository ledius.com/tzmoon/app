import { UserNotFoundByIdException } from './user-not-found-by-id-exception';

describe('UserNotFoundByIdException', () => {
  it('should be defined', () => {
    expect(new UserNotFoundByIdException("1234")).toBeDefined();
  });
});
