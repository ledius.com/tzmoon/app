import { UserLoginAlreadyExistsException } from './user-login-already-exists-exception';

describe('UserLoginAlreadyExistsException', () => {
  it('should be defined', () => {
    expect(new UserLoginAlreadyExistsException("test")).toBeDefined();
  });
});
