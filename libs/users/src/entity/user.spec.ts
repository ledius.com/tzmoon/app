import { User } from './user';

describe('User', () => {
  it('should be defined', () => {
    expect(new User("test", "same")).toBeDefined();
  });

  it("should be return correct input data", () => {
    const [id, login] = ["test", "same"];
    const user = new User(id, login);
    expect(user.getId()).toBe(id);
    expect(user.getLogin()).toBe(login);
  });
});
