import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";
import { Exclude, Expose } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";
import { v4 } from "uuid";

@Entity("users")
export class User {

    @PrimaryGeneratedColumn('uuid')
    @Exclude()
    private readonly id: string;

    @Column({
        unique: true
    })
    @Exclude()
    private login: string;

    /**
     *
     * @param id
     * @param login
     */
    constructor(id: string, login: string) {
        this.id = id;
        this.login = login;
    }

    @ApiProperty({
        name: 'id',
        type: 'string',
        example: v4(),
    })
    @Expose({
        name: 'id'
    })
    public getId(): string {
        return this.id;
    }

    @ApiProperty({
        name: 'login',
        type: 'string',
        example: 'test.login'
    })
    @Expose({
        name: 'login',
    })
    public getLogin(): string {
        return this.login;
    }

    public changeLogin(login: string): void {
        this.login = login;
    }
}
