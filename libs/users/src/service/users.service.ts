import { Injectable, NotImplementedException } from '@nestjs/common';
import { CreateUserDto } from "@app/users/dto/create-user-dto";
import { InjectUsersRepository, UsersRepositoryInterface } from "@app/users/repository/users-repository.interface";
import { User } from "@app/users/entity/user";
import * as uuid from "uuid";
import { UserLoginAlreadyExistsException } from "@app/users/exception/user-login-already-exists-exception";

@Injectable()
export class UsersService {

    constructor(
        @InjectUsersRepository() private readonly users: UsersRepositoryInterface
    ) {}

    public async findByLogin(login: string): Promise<User|undefined> {
        return this.users.findByLogin(login);
    }

    public async create(dto: CreateUserDto): Promise<User> {
        const found = await this.users.findByLogin(dto.login);
        if(found) {
            throw new UserLoginAlreadyExistsException(dto.login);
        }
        const id = dto.id || uuid.v4();
        const user = new User(id, dto.login);
        await this.users.save(user);
        return user;
    }

    public async getById(id: string): Promise<User> {
        return this.users.getById(id);
    }

    public async remove(user: User): Promise<void> {
        await this.users.remove(user);
    }
}
