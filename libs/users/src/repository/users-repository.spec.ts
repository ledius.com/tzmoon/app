import { UsersRepository } from './users-repository';
import { Connection } from "typeorm";

describe('UsersRepository', () => {
  it('should be defined', () => {
    const mockConnection = {
      getRepository: jest.fn().mockImplementation()
    } as unknown as Connection;
    expect(new UsersRepository(mockConnection)).toBeDefined();
  });
});
