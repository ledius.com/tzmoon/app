import { Injectable } from "@nestjs/common";
import { Connection, Repository } from "typeorm";
import { User, UsersRepositoryInterface } from "@app/users";
import { UserNotFoundByIdException } from "@app/users/exception/user-not-found-by-id-exception";

@Injectable()
export class UsersRepository implements UsersRepositoryInterface {

    private readonly repository: Repository<User> = this.connection.getRepository(User);

    constructor(
        private readonly connection: Connection
    ) {}

    /**
     *
     * @param login
     */
    public async findByLogin(login: string): Promise<User | undefined> {
        return this.repository.findOne({
            where: {
                login,
            }
        })
    }

    /**
     *
     * @param user
     */
    public async save(user: User): Promise<void> {
        await this.repository.save(user);
    }

    public async getById(id: string): Promise<User> {
        const found = await this.repository.findOne(id);
        if(!found) {
            throw new UserNotFoundByIdException(id);
        }
        return found;
    }

    public async remove(user: User): Promise<void> {
        await this.repository.remove(user);
    }


}
