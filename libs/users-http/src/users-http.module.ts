import { Module } from '@nestjs/common';
import { UsersController } from "@app/users-http/controller/users/users.controller";
import { UsersModule } from "@app/users";

@Module({
  imports: [
      UsersModule
  ],
  controllers: [
      UsersController
  ],
  exports: [
      UsersModule
  ]
})
export class UsersHttpModule {}
