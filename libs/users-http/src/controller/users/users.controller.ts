import { Body, Controller, Post } from '@nestjs/common';
import { ApiCreatedResponse, ApiTags } from "@nestjs/swagger";
import { CreateUserDto, User, UsersService } from "@app/users";

@Controller('users')
@ApiTags("Users")
export class UsersController {
    constructor(
        private readonly usersService: UsersService,
    ) {}

    /**
     *
     * @param dto
     */
    @Post()
    @ApiCreatedResponse({
        type: User
    })
    public async create(@Body() dto: CreateUserDto): Promise<User> {
        return await this.usersService.create(dto);
    }
}
