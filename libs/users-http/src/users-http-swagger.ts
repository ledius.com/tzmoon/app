import { INestApplication } from "@nestjs/common";
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from "@nestjs/swagger";
import { UsersHttpModule } from "@app/users-http/users-http.module";

export function UsersHttpSwagger(app: INestApplication): OpenAPIObject {
    const options = new DocumentBuilder()
        .setTitle("Users")
        .setVersion("1.0")
        .build();
    const document = SwaggerModule.createDocument(app, options, {
        include: [
            UsersHttpModule
        ]
    });
    SwaggerModule.setup("api/users/docs", app, document);
    return document;
}
