import { Module } from '@nestjs/common';
import { AuthenticationController } from './controller/authentication/authentication.controller';
import { AuthenticationModule } from "@app/authentication";

@Module({
  imports: [
      AuthenticationModule,
  ],
  controllers: [AuthenticationController],
})
export class AuthenticationHttpModule {}
