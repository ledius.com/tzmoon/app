import { DevelopersProductRepository } from './developers-product-repository';
import { Connection } from "typeorm";

describe('DevelopersProductRepository', () => {
  it('should be defined', () => {
    expect(new DevelopersProductRepository({} as Connection)).toBeDefined();
  });
});
