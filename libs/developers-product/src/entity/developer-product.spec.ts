import { DeveloperProduct } from './developer-product';

describe('DeveloperProduct', () => {
  it('should be defined', () => {
    const developer = new DeveloperProduct("id", "ddd", "ddd", 100, 200)
    expect(developer).toBeDefined();
  });

  it("should be filled correct", () => {
    const developer = new DeveloperProduct("id", "same-name", "avatar", 100, 202);
    expect(developer.getId()).toBe("id");
    expect(developer.getName()).toBe("same-name");
    expect(developer.getAvatar()).toBe("avatar");
    expect(developer.getPerformance()).toBe(100);
    expect(developer.getPrice()).toBe(202);
  });

  describe("change methods", () => {
    let developer: DeveloperProduct;
    beforeEach(() => {
      developer = new DeveloperProduct(
          "sss",
          "same-name",
          "avatar",
          120,
          300
      );
    });

    it("should change name", () => {
      const newName = "its-name";
      developer.changeName(newName);
      expect(developer.getName()).toBe(newName);
    });

    it("should change avatar", () => {
      const newAvatar = "https://avatar.com/1234";
      developer.changeAvatar(newAvatar);
      expect(developer.getAvatar()).toBe(newAvatar);
    });

    it("should change performance", () => {
      const newPerformance = 22312;
      developer.changePerformance(newPerformance);
      expect(developer.getPerformance()).toBe(newPerformance);
    });

    it('should change price', () => {
      const newPrice = 309;
      developer.changePrice(newPrice);
      expect(developer.getPrice()).toBe(newPrice);
    });
  });
});
