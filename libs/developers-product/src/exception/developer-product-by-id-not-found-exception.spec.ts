import { DeveloperProductByIdNotFoundException } from './developer-product-by-id-not-found-exception';

describe('DeveloperProductByIdNotFoundException', () => {
  it('should be defined', () => {
    expect(new DeveloperProductByIdNotFoundException("123")).toBeDefined();
  });
});
