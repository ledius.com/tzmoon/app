import { v4 } from "uuid";
import {
    DevelopersProductRepositoryInterface,
    InjectDevelopersProductRepository
} from "./../repository/developers-product-repository.interface";
import { DeveloperProduct } from "./../entity/developer-product";
import { CreateDeveloperProductDto } from "@app/developers-product/dto/create-developer-product-dto";
import { Injectable } from "@nestjs/common";


@Injectable()
export class DevelopersProductService {
    constructor(
        @InjectDevelopersProductRepository() private readonly developersProduct: DevelopersProductRepositoryInterface
    ) {}

    public async create(dto: CreateDeveloperProductDto): Promise<DeveloperProduct> {
        const id = v4();
        const developerProduct = new DeveloperProduct(id, dto.name, dto.avatar, dto.performance, dto.price);
        await this.developersProduct.save(developerProduct);
        return developerProduct;
    }

    public async find(): Promise<DeveloperProduct[]> {
        return await this.developersProduct.find();
    }

    public async getById(id: string): Promise<DeveloperProduct> {
        return await this.developersProduct.getById(id);
    }

    public async findByIds(ids: string[]): Promise<DeveloperProduct[]> {
        return await this.developersProduct.findByIds(ids);
    }

    public async remove(id: string): Promise<void> {
        const developerProduct = await this.developersProduct.getById(id);
        await this.developersProduct.remove(developerProduct);
    }
}
