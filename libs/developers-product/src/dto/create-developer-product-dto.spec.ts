import { CreateDeveloperProductDto } from './create-developer-product-dto';

describe('CreateDeveloperProductDto', () => {
  it('should be defined', () => {
    expect(new CreateDeveloperProductDto()).toBeDefined();
  });
});
