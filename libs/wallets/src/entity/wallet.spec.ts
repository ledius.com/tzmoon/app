import { Wallet } from './wallet';
import { User } from "@app/users";

describe('Wallet', () => {
  it('should be defined', () => {
    expect(Wallet.create("1234", {} as User, 10)).toBeDefined();
  });

  describe("create", () => {
    it("should be create wallet with correct values", () => {
      const user = new User("ddawd", "ddwad");
      const wallet = Wallet.create("1234", user, 10);
      expect(wallet.getUser()).toEqual(user);
      expect(wallet.getBalance()).toBe(10);
    })
  });
});
