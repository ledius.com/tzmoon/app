import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Exclude, Expose, Type } from "class-transformer";
import { ApiProperty } from "@nestjs/swagger";
import { User } from "@app/users";

@Entity('wallets')
export class Wallet {

    @PrimaryGeneratedColumn('uuid')
    @Exclude({
        toPlainOnly: true
    })
    private readonly id: string;

    @Column({
        type: 'numeric',
        precision: 10,
        scale: 2,
        unsigned: true
    })
    @Exclude({
        toPlainOnly: true
    })
    private balance: number|string;

    @ManyToOne(() => User, {
        onDelete: "CASCADE",
        onUpdate: "CASCADE",
        eager: true,
    })
    @JoinColumn({
        name: 'users_id',
        referencedColumnName: 'id'
    })
    @Exclude({
        toPlainOnly: true
    })
    private user: User;

    private constructor(id: string) {
        this.id = id;
        this.balance = 0;
    }

    public static create(id: string, user: User, balance: number): Wallet {
        const self = new Wallet(id);
        self.user = user;
        self.changeBalance(balance);
        return self;
    }

    @ApiProperty({
        name: 'id',
        type: 'string'
    })
    @Expose({
        name: 'id'
    })
    public getId(): string {
        return this.id;
    }

    @ApiProperty({
        name: 'balance',
        type: 'number'
    })
    @Expose({
        name: 'balance'
    })
    public getBalance(): number {
        return Number(this.balance);
    }

    @ApiProperty({
        name: 'user',
        type: User,
    })
    @Expose({
        name: 'user'
    })
    @Type(() => User)
    public getUser(): User {
        return this.user;
    }

    public changeBalance(balance: number): void {
        this.balance = balance;
    }

    public deposit(amount: number): void {
        this.balance = Number(this.balance) + amount;
    }

    public withdraw(amount: number): void {
        this.balance = Number(this.balance) - amount;
    }
}
