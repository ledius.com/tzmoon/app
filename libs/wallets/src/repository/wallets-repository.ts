import { Injectable } from "@nestjs/common";
import { Connection, Repository } from "typeorm";
import { Wallet } from "@app/wallets/entity/wallet";
import { WalletRepositoryInterface } from "@app/wallets/repository/wallet-repository.interface";
import { WalletByIdNotFoundException } from "@app/wallets/exception/wallet-by-id-not-found-exception";

@Injectable()
export class WalletsRepository implements WalletRepositoryInterface {

    private readonly repository: Repository<Wallet> = this.connection.getRepository(Wallet);

    constructor(
        private readonly connection: Connection
    ) {}

    public async getById(id: string): Promise<Wallet> {
        const found = await this.repository.findOne(id);
        if(!found) {
            throw new WalletByIdNotFoundException(id);
        }
        return found;
    }

    public async getByUserId(userId: string): Promise<Wallet[]> {
        return await this.repository.find({
            where: {
                user: {
                    id: userId
                }
            }
        })
    }

    public async remove(wallet: Wallet): Promise<void> {
        await this.repository.remove(wallet);
    }

    public async save(wallet: Wallet): Promise<void> {
        await this.repository.save(wallet);
    }

    public async update(wallet: Wallet): Promise<void> {
        await this.repository.save(wallet);
    }
}
