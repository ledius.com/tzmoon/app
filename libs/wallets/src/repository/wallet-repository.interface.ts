import { Inject } from "@nestjs/common";
import { Wallet } from "@app/wallets/entity/wallet";

export interface WalletRepositoryInterface {
    getByUserId(userId: string): Promise<Wallet[]>;
    save(wallet: Wallet): Promise<void>;
    update(wallet: Wallet): Promise<void>;
    getById(id: string): Promise<Wallet>;
    remove(wallet: Wallet): Promise<void>;
}

export const WALLET_REPOSITORY_INTERFACE = "WalletRepositoryInterface";

export const InjectWalletRepository = Inject.bind(null, WALLET_REPOSITORY_INTERFACE);
