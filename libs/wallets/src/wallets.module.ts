import { Module } from '@nestjs/common';
import { WalletsService } from './service/wallets.service';
import { WALLET_REPOSITORY_INTERFACE } from "@app/wallets/repository/wallet-repository.interface";
import { WalletsRepository } from "@app/wallets/repository/wallets-repository";
import { UsersModule } from "@app/users";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Wallet } from "@app/wallets/entity/wallet";
import { UserWalletCreateListener } from "@app/wallets/listener/user-wallet-create-listener";

@Module({
  imports: [
      TypeOrmModule.forFeature([
          Wallet,
      ]),
      UsersModule,
  ],
  providers: [
      WalletsService,
      WalletsRepository,
      UserWalletCreateListener,
      {
          provide: WALLET_REPOSITORY_INTERFACE,
          useClass: WalletsRepository
      },
  ],
  exports: [WalletsService],
})
export class WalletsModule {}
