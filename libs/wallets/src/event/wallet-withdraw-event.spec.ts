import { WalletWithdrawEvent } from './wallet-withdraw-event';
import { Wallet } from "@app/wallets/entity/wallet";
import { User } from "@app/users";

describe('WalletWithdrawEvent', () => {
  it('should be defined', () => {
    expect(new WalletWithdrawEvent(
        Wallet.create("123", {} as User, 10),
        10
    )).toBeDefined();
  });
});
