import { Wallet } from "@app/wallets/entity/wallet";

export class WalletCreatedEvent {

    public static readonly event: string = 'wallet.created';

    constructor(
        private readonly wallet: Wallet
    ) {}

    public getWallet(): Wallet {
        return this.wallet;
    }
}
