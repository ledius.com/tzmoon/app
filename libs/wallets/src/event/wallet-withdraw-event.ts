import { Wallet } from "@app/wallets/entity/wallet";

export class WalletWithdrawEvent {
    public static readonly event: string = 'wallet.withdraw';

    constructor(
        private readonly wallet: Wallet,
        private readonly amount: number
    ) {}

    public getWallet(): Wallet {
        return this.wallet;
    }

    public getAmount(): number {
        return this.amount;
    }
}
