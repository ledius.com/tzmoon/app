import { Wallet } from "@app/wallets/entity/wallet";

export class WalletDepositEvent {

    public static readonly event: string = 'wallet.deposit';

    constructor(
        private readonly wallet: Wallet,
        private readonly amount: number
    ) {}

    public getWallet(): Wallet {
        return this.wallet;
    }

    public getAmount(): number {
        return this.amount;
    }
}
