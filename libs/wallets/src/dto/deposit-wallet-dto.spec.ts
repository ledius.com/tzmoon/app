import { DepositWalletDto } from './deposit-wallet-dto';

describe('DepositWalletDto', () => {
  it('should be defined', () => {
    expect(new DepositWalletDto()).toBeDefined();
  });
});
