import { UserWalletCreateListener } from './user-wallet-create-listener';
import { WalletsService } from "@app/wallets";
import { EVENT_LISTENER_METADATA } from "@nestjs/event-emitter/dist/constants";
import { SignupUserEvent } from "@app/signup/event/signup-user-event";
import { User } from "@app/users";
import { Password } from "@app/passwords";
import { CreateWalletDto } from "@app/wallets/dto/create-wallet-dto";

describe('UserWalletCreateListener', () => {

  let wallets: WalletsService;
  let listener: UserWalletCreateListener;

  beforeEach(() => {
    wallets = {
      getByUserId: jest.fn(),
    } as unknown as WalletsService;
    listener = new UserWalletCreateListener(wallets);
  })

  it('should be defined', () => {
    expect(listener).toBeDefined();
  });

  it("should listen event", () => {
    const metadata: Record<string, unknown> = Reflect.getMetadata(EVENT_LISTENER_METADATA, listener.listen);
    expect(metadata).toBeDefined();
    expect(metadata.event).toBe(SignupUserEvent.event);
    expect(metadata.options).toBeUndefined();
  })

  it("should be create wallet for user if wallets doest not exists", async () => {
    const expectedUser = new User("dawd", "daadw");
    const password = new Password("dddd", "ddsad", expectedUser);
    const event = new SignupUserEvent(expectedUser, password);
    wallets.getByUserId = jest.fn().mockImplementation(uid => {
      expect(uid).toBe(expectedUser.getId());
      return [];
    });
    wallets.create = jest.fn().mockImplementation((dto: CreateWalletDto) => {
      expect(dto.user).toBe(expectedUser.getId());
    });
    await listener.listen(event);
    expect(wallets.create).toBeCalled();
    expect(wallets.getByUserId).toBeCalled();
  });
});
