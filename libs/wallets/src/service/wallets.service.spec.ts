import { Test, TestingModule } from '@nestjs/testing';
import { WalletsService } from './wallets.service';
import { EventEmitter2 } from "@nestjs/event-emitter";
import {
  WALLET_REPOSITORY_INTERFACE,
  WalletRepositoryInterface
} from "@app/wallets/repository/wallet-repository.interface";
import { User, UsersService } from "@app/users";
import { WalletCreatedEvent } from "@app/wallets/event/wallet-created-event";
import { Wallet } from "@app/wallets/entity/wallet";
import { CreateWalletDto } from "@app/wallets/dto/create-wallet-dto";
import { WalletDepositEvent } from "@app/wallets/event/wallet-deposit-event";
import { DepositWalletDto } from "@app/wallets/dto/deposit-wallet-dto";
import { WithdrawWalletDto } from "@app/wallets/dto/withdraw-wallet-dto";

describe('WalletsService', () => {
  let service: WalletsService;
  let emitter: EventEmitter2;
  let walletRepository: WalletRepositoryInterface;
  let userService: UsersService;

  beforeEach(async () => {
    emitter = new EventEmitter2();
    walletRepository = {
      save: jest.fn().mockImplementation(),
    } as unknown as WalletRepositoryInterface;
    userService = {} as unknown as UsersService;
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        WalletsService,
        {
          provide: EventEmitter2,
          useValue: emitter,
        },
        {
          provide: WALLET_REPOSITORY_INTERFACE,
          useValue: walletRepository
        },
        {
          provide: UsersService,
          useValue: userService,
        }
      ],
    }).compile();

    service = module.get<WalletsService>(WalletsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe("create", () => {
    it("should be create a wallet", async () => {
      const expectedUser = new User("test", "same");
      userService.getById = jest.fn().mockImplementation(async () => expectedUser);
      emitter.addListener(WalletCreatedEvent.event, async (payload: WalletCreatedEvent) => {
        expect(payload.getWallet()).toBeInstanceOf(Wallet);
        expect(payload.getWallet().getUser()).toEqual(expectedUser);
        expect(payload.getWallet().getBalance()).toBe(0);
        expect(payload.getWallet().getId()).toBeDefined();
      });
      const dto = {
        user: expectedUser.getId()
      } as CreateWalletDto;
      const wallet = await service.create(dto);
      expect(wallet).toBeInstanceOf(Wallet);
      expect(wallet.getUser()).toEqual(expectedUser);
      expect(wallet.getBalance()).toBe(0);
      expect(wallet.getId()).toBeDefined();
    });
  });

  describe("deposit", () => {
    it("should be increase amount balance", async () => {
      const expectedWallet = Wallet.create("1234", new User("123", "123"), 0);
      const dto = {
        amount: 10
      } as DepositWalletDto;
      walletRepository.getById = jest.fn().mockImplementation(async () => expectedWallet);
      walletRepository.update = jest.fn();
      emitter.addListener(WalletDepositEvent.event, async (payload: WalletDepositEvent) => {
        expect(payload.getWallet().getUser()).toEqual(expectedWallet.getUser());
        expect(payload.getWallet().getId()).toBe(expectedWallet.getId());
        expect(payload.getWallet().getBalance()).toBe(10);
      });
      const wallet = await service.deposit(expectedWallet.getId(), dto);
      expect(wallet.getBalance()).toBe(10);
      expect(wallet).toEqual(expectedWallet);
      expect(walletRepository.getById).toHaveBeenCalled();
      expect(walletRepository.update).toHaveBeenCalled();
    });
  })

  describe("withdraw", () => {
    it("should be subtract amount balance", async () => {
      const expectedWallet = Wallet.create("333", new User("123", "123"), 30);
      const dto = {
        amount: 12
      } as WithdrawWalletDto;
      walletRepository.getById = jest.fn().mockImplementation(async () => expectedWallet);
      walletRepository.update = jest.fn();
      emitter.addListener(WalletDepositEvent.event, async (payload: WalletDepositEvent) => {
        expect(payload.getWallet().getUser()).toEqual(expectedWallet.getUser());
        expect(payload.getWallet().getId()).toBe(expectedWallet.getId());
        expect(payload.getWallet().getBalance()).toBe(18);
      });
      const wallet = await service.withdraw(expectedWallet.getId(), dto);
      expect(wallet.getBalance()).toBe(18);
      expect(wallet).toEqual(expectedWallet);
      expect(walletRepository.getById).toHaveBeenCalled();
      expect(walletRepository.update).toHaveBeenCalled();
    });
  });

  describe("getByUserId", () => {
    it("should be return wallets by user id", async () => {
      const userId = 'test-id';
      const expectedWallets = [
          Wallet.create("123", {} as User, 0),
          Wallet.create("1234", {} as User, 10),
          Wallet.create("1253", {} as User, 22),
      ];
      walletRepository.getByUserId = jest.fn().mockImplementation(async (userId: string) => {
        expect(userId).toBe(userId);
        return expectedWallets;
      });
      const wallets = await service.getByUserId(userId);
      expect(wallets).toEqual(expectedWallets);
    });
  });

  describe("remove", () => {
    it("should be get by id and remove wallet", async () => {
      const walletId = 'same-id';
      const expectedWallet = Wallet.create("1234", {} as User, 11);
      walletRepository.remove = jest.fn().mockImplementation(async wallet => {
        expect(wallet).toEqual(expectedWallet);
      });
      walletRepository.getById = jest.fn().mockImplementation(async (id: string) => {
        expect(id).toBe(walletId);
        return expectedWallet;
      });
      await service.remove(walletId);
    });
  });
});
