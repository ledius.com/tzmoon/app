import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { WalletsService } from "@app/wallets";
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiTags } from "@nestjs/swagger";
import { Wallet } from "@app/wallets/entity/wallet";
import { CreateWalletDto } from "@app/wallets/dto/create-wallet-dto";
import { DepositWalletDto } from "@app/wallets/dto/deposit-wallet-dto";
import { WithdrawWalletDto } from "@app/wallets/dto/withdraw-wallet-dto";
import { JwtAuthGuard } from "@app/authentication/guard/jwt-auth-guard";
import { AuthUser } from "@app/authentication/decorator/auth-user.decorator";
import { User } from "@app/users";

@Controller()
@ApiTags("Wallets")
export class WalletsController {
    constructor(
        private readonly wallets: WalletsService,
    ) {}

    @Post("wallet")
    @ApiCreatedResponse({
        type: Wallet
    })
    @ApiBearerAuth()
    @UseGuards(JwtAuthGuard)
    public async create(@AuthUser() user: User, @Body() dto: CreateWalletDto): Promise<Wallet> {
        return await this.wallets.create(dto);
    }

    @Patch("wallet/:walletId/deposit")
    @ApiOkResponse({
        type: Wallet
    })
    public async deposit(
        @Param("walletId") walletId: string,
        @Body() dto: DepositWalletDto
    ): Promise<Wallet> {
        return await this.wallets.deposit(walletId, dto);
    }

    @Patch("wallet/:walletId/withdraw")
    @ApiOkResponse({
        type: Wallet
    })
    public async withdraw(
        @Param("walletId") walletId: string,
        @Body() dto: WithdrawWalletDto
    ): Promise<Wallet> {
        return await this.wallets.withdraw(walletId, dto);
    }

    @Get(":userId/wallets")
    @ApiOkResponse({
        type: Wallet,
        isArray: true
    })
    public async getByUserId(@Param("userId") userId: string): Promise<Wallet[]> {
        return await this.wallets.getByUserId(userId);
    }

    @Delete("wallet/:walletId")
    @ApiOkResponse()
    public async remove(@Param("walletId") walletId: string): Promise<void> {
        await this.wallets.remove(walletId);
    }
}
