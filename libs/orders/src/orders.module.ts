import { Module } from '@nestjs/common';
import { OrdersService } from './service/orders.service';
import { WalletsModule } from "@app/wallets";
import { DevelopersModule } from "@app/developers";
import { DevelopersProductModule } from "@app/developers-product";

@Module({
  imports: [
      WalletsModule,
      DevelopersModule,
      DevelopersProductModule,
  ],
  providers: [OrdersService],
  exports: [OrdersService],
})
export class OrdersModule {}
