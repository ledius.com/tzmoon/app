import { Injectable } from '@nestjs/common';
import { DevelopersProductService } from "@app/developers-product/service/developers-product.service";
import { DevelopersService } from "@app/developers/service/developers.service";
import { Developer } from "@app/developers/entity/developer";
import { BuyDeveloperDto } from "@app/orders/dto/buy-developer-dto";
import { WalletsService } from "@app/wallets/service/wallets.service";

@Injectable()
export class OrdersService {
    constructor(
        private readonly wallets: WalletsService,
        private readonly products: DevelopersProductService,
        private readonly developers: DevelopersService
    ) {}

    public async buy(dto: BuyDeveloperDto): Promise<Developer[]> {
        const products = await this.products.findByIds(dto.products);
        const wallets = await this.wallets.getByUserId(dto.user);
        const wallet = wallets[0];
        if(!wallet) {
            throw new Error("Wallet not found");
        }
        const developers = [];
        for(const product of products) {
            const developer = await this.developers.create({
                performance: product.getPerformance(),
                price: product.getPrice(),
                name: product.getName(),
                avatar: product.getAvatar(),
                owner: dto.user,
            });
            try {
                await this.wallets.withdraw(wallet.getId(), {
                    amount: product.getPrice()
                });
                developers.push(developer);
            } catch (e) {
                await this.developers.remove(developer.getId());
            }
        }
        return developers;
    }
}
