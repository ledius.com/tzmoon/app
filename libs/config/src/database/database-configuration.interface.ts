import { PostgresConfiguration } from "@app/config/database/postgres-configuration.interface";

export interface DatabaseConfiguration {
    postgres: PostgresConfiguration;
}
