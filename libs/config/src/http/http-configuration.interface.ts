export interface HttpConfiguration {
    host: string;
    port: number;
}
