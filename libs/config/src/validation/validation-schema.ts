import * as Joi from "@hapi/joi"
import { ObjectSchema } from "@hapi/joi";

export class ValidationSchema {
    validate(value: Record<string, unknown>): void {
        Joi.assert(value, this.get());
    }
    get(): ObjectSchema {
        return Joi.object({
            auth: Joi.object({
                secret: Joi.string().required().min(10),
            }),
            startup: Joi.object({
                timeout: Joi.number().integer().positive().required()
            }).required(),
            http: Joi.object({
                host: Joi.string().hostname().required(),
                port: Joi.number().port().required()
            }).required(),
            database: Joi.object({
                postgres: Joi.object({
                    user: Joi.string().required(),
                    password: Joi.string().required(),
                    database: Joi.string().required(),
                    port: Joi.number().port().required(),
                    host: Joi.string().required(),
                }).required()
            }).required()
        })
    }
}
