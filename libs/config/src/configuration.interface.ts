import { HttpConfiguration } from "@app/config/http/http-configuration.interface";
import { StartupConfiguration } from "@app/config/startup/startup-configuration.interface";
import { DatabaseConfiguration } from "@app/config/database/database-configuration.interface";

export interface Configuration {
    http: HttpConfiguration;
    startup: StartupConfiguration;
    database: DatabaseConfiguration
}
