import { Module } from '@nestjs/common';
import { DevelopersProductController } from './controller/developers-product/developers-product.controller';
import { DevelopersProductModule } from "@app/developers-product";

@Module({
  imports: [
      DevelopersProductModule,
  ],
  controllers: [DevelopersProductController],
})
export class DevelopersProductHttpModule {}
