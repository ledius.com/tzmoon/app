import { Test, TestingModule } from '@nestjs/testing';
import { DevelopersProductController } from './developers-product.controller';
import { DevelopersProductService } from "./../../../../developers-product/src/service/developers-product.service";

describe('DevelopersProductController', () => {
  let controller: DevelopersProductController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [DevelopersProductController],
      providers: [
          {
              provide: DevelopersProductService,
              useValue: {}
          }
      ]
    }).compile();

    controller = module.get<DevelopersProductController>(DevelopersProductController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
