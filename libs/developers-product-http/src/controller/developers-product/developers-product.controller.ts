import { Body, Controller, Delete, Get, Param, Post } from '@nestjs/common';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from "@nestjs/swagger";
import { DeveloperProduct, DevelopersProductService } from "./../../../../developers-product/src/index";
import { CreateDeveloperProductDto } from "./../../../../developers-product/src/dto/create-developer-product-dto";

@Controller()
@ApiTags("Developers Product")
export class DevelopersProductController {
    constructor(
        private readonly developersProduct: DevelopersProductService
    ) {}

    @Post("developers-product")
    @ApiCreatedResponse({
        type: DeveloperProduct
    })
    public async create(@Body() dto: CreateDeveloperProductDto): Promise<DeveloperProduct> {
        return this.developersProduct.create(dto);
    }

    @Get("developers-product/:developerProductId")
    @ApiOkResponse({
        type: DeveloperProduct
    })
    public async getById(@Param("developerProductId") developerProductId: string): Promise<DeveloperProduct> {
        return this.developersProduct.getById(developerProductId);
    }

    @Get('developers-products')
    @ApiOkResponse({
        type: DeveloperProduct,
        isArray: true
    })
    public async find(): Promise<DeveloperProduct[]> {
        return this.developersProduct.find();
    }

    @Delete("developers-product/:developerProductId")
    @ApiOkResponse()
    public async remove(@Param("developerProductId") developerProductId: string): Promise<void> {
        await this.developersProduct.remove(developerProductId);
    }
}
