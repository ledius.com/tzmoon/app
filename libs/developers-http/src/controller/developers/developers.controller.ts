import { Body, Controller, Delete, Get, Param, Post, UseGuards } from '@nestjs/common';
import { ApiBearerAuth, ApiCreatedResponse, ApiOkResponse, ApiTags } from "@nestjs/swagger";
import { DevelopersService } from "@app/developers";
import { Developer } from "@app/developers/entity/developer";
import { CreateDeveloperDto } from "@app/developers/dto/create-developer-dto";
import { JwtAuthGuard } from "@app/authentication/guard/jwt-auth-guard";

@Controller()
@ApiTags("Developers")
export class DevelopersController {
    constructor(
        private readonly developers: DevelopersService,
    ) {}

    @Post("developer")
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    @ApiCreatedResponse({
        type: Developer
    })
    public async create(@Body() dto: CreateDeveloperDto): Promise<Developer> {
        return this.developers.create(dto);
    }

    @Get(":ownerId/developers")
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    @ApiOkResponse({
        type: Developer,
        isArray: true
    })
    public async getByOwner(@Param("ownerId") ownerId: string): Promise<Developer[]> {
        return await this.developers.getByOwnerId(ownerId);
    }

    @Get("developers")
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    @ApiOkResponse({
        type: Developer,
        isArray: true
    })
    public async find(): Promise<Developer[]> {
        return await this.developers.find();
    }

    @Delete("developer/:developerId")
    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth()
    @ApiOkResponse()
    public async remove(@Param("developerId") developerId: string): Promise<void> {
        await this.developers.remove(developerId);
    }
}
