import { Module } from '@nestjs/common';
import { DevelopersController } from './controller/developers/developers.controller';
import { DevelopersModule } from "@app/developers";

@Module({
  imports: [
      DevelopersModule,
  ],
  controllers: [DevelopersController],
})
export class DevelopersHttpModule {}
