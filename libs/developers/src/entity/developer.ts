import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Exclude, Expose, Type } from "class-transformer";
import { User } from "@app/users";
import { ApiProperty } from "@nestjs/swagger";

@Entity('developers')
export class Developer {

    @PrimaryGeneratedColumn('uuid')
    @Exclude()
    private readonly id: string;

    @Column()
    @Exclude()
    private name: string;

    @Column()
    @Exclude()
    private avatar: string;

    @Column({
        type: 'int',
        unsigned: true
    })
    @Exclude()
    private performance: number;


    @Column({
        type: 'numeric',
        precision: 10,
        scale: 2
    })
    @Exclude()
    private price: number;

    @ManyToOne(() => User, {
        onUpdate: "CASCADE",
        onDelete: "CASCADE",
        eager: true
    })
    @JoinColumn({
        name: 'users_id',
        referencedColumnName: 'id',
    })
    @Exclude()
    private owner: User;

    constructor(id: string, name: string, avatar: string, performance: number, price: number, owner: User) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.performance = performance;
        this.price = price;
        this.owner = owner;
    }

    @ApiProperty({
        name: 'id',
        type: 'string',
        format: 'uuid'
    })
    @Expose({
        name: 'id'
    })
    public getId(): string {
        return this.id;
    }

    @ApiProperty({
        name: 'name',
        type: 'string',
        format: 'name'
    })
    @Expose({
        name: 'name'
    })
    public getName(): string {
        return this.name;
    }

    @ApiProperty({
        name: 'avatar',
        type: 'string',
        format: 'url'
    })
    @Expose({
        name: 'avatar'
    })
    public getAvatar(): string {
        return this.avatar;
    }

    @ApiProperty({
        name: 'performance',
        type: 'number'
    })
    @Expose({
        name: 'performance'
    })
    public getPerformance(): number {
        return this.performance;
    }

    @ApiProperty({
        name: 'price',
        type: 'number'
    })
    @Expose({
        name: 'price'
    })
    public getPrice(): number {
        return this.price;
    }

    @ApiProperty({
        name: 'owner',
        type: User,
    })
    @Expose({
        name: 'owner'
    })
    @Type(() => User)
    public getOwner(): User {
        return this.owner;
    }

    public changeName(name: string): void {
        this.name = name;
    }

    public changeAvatar(avatar: string): void {
        this.avatar = avatar;
    }

    public changePerformance(performance: number): void {
        this.performance = performance;
    }

    public changePrice(price: number): void {
        this.price = price;
    }

    public changeOwner(owner: User): void {
        this.owner = owner;
    }
}
