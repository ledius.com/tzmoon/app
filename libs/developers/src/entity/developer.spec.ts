import { Developer } from './developer';
import { User } from "@app/users";

describe('Developer', () => {
  it('should be defined', () => {
    expect(new Developer(
        'dawd',
        "dawdw",
        "dawd",
        10,
        201,
        new User("wad", "dawd")
    )).toBeDefined();
  });

  it("should be filled correct", () => {
    const user = new User("same", "login");
    const developer = new Developer("ud", "same-name", "avatar", 100, 202, user);
    expect(developer.getId()).toBe("ud");
    expect(developer.getName()).toBe("same-name");
    expect(developer.getAvatar()).toBe("avatar");
    expect(developer.getPerformance()).toBe(100);
    expect(developer.getPrice()).toBe(202);
    expect(developer.getOwner()).toEqual(user);
  });

  describe("change methods", () => {
    let developer: Developer;
    beforeEach(() => {
      developer = new Developer(
          "sss",
          "same-name",
          "avatar",
          120,
          300,
          new User("123", "222")
      );
    });

    it("should change name", () => {
      const newName = "its-name";
      developer.changeName(newName);
      expect(developer.getName()).toBe(newName);
    });

    it("should change avatar", () => {
      const newAvatar = "https://avatar.com/1234";
      developer.changeAvatar(newAvatar);
      expect(developer.getAvatar()).toBe(newAvatar);
    });

    it("should change performance", () => {
      const newPerformance = 22312;
      developer.changePerformance(newPerformance);
      expect(developer.getPerformance()).toBe(newPerformance);
    });

    it('should change price', () => {
      const newPrice = 309;
      developer.changePrice(newPrice);
      expect(developer.getPrice()).toBe(newPrice);
    })

    it("should change owner", () => {
      const newOwner = new User("123123", "newLog");
      developer.changeOwner(newOwner);
      expect(developer.getOwner()).toBe(newOwner);
    });
  });
});
