import { Module } from '@nestjs/common';
import { DevelopersService } from './service/developers.service';
import { DevelopersRepository } from "@app/developers/repository/developers-repository";
import { DEVELOPERS_REPOSITORY_INTERFACE } from "@app/developers/repository/developers-repository.interface";
import { UsersModule } from "@app/users";

@Module({
    imports: [
        UsersModule,
    ],
    providers: [
        DevelopersService,
        DevelopersRepository,
        {
            provide: DEVELOPERS_REPOSITORY_INTERFACE,
            useExisting: DevelopersRepository
        }
    ],
    exports: [DevelopersService],
})
export class DevelopersModule {}
