import { Test, TestingModule } from '@nestjs/testing';
import { DevelopersService } from './developers.service';
import {
  DEVELOPERS_REPOSITORY_INTERFACE,
  DevelopersRepositoryInterface
} from "@app/developers/repository/developers-repository.interface";
import { User, UsersService } from "@app/users";
import { CreateDeveloperDto } from "@app/developers/dto/create-developer-dto";
import { Developer } from "@app/developers/entity/developer";
import { EventEmitter2 } from "@nestjs/event-emitter";
import { DeveloperCreatedEvent } from "@app/developers/event/developer-created-event";

describe('DevelopersService', () => {
  let service: DevelopersService;
  let developersRepository: DevelopersRepositoryInterface;
  let usersService: UsersService;
  let emitter: EventEmitter2;

  beforeEach(async () => {
    developersRepository = {} as unknown as DevelopersRepositoryInterface;
    usersService = {} as unknown as UsersService;
    emitter = new EventEmitter2();
    const module: TestingModule = await Test.createTestingModule({
      providers: [
          DevelopersService,
          {
              provide: EventEmitter2,
              useValue: emitter
          },
          {
              provide: DEVELOPERS_REPOSITORY_INTERFACE,
              useValue: developersRepository
          },
          {
              provide: UsersService,
              useValue: usersService
          }
      ],
    }).compile();

    service = module.get<DevelopersService>(DevelopersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe("create", () => {
      it("should be create developer", async () => {
          const expectedUser = new User("same", "some");
          const dto = {
              performance: 20,
              avatar: "https://same.com",
              owner: expectedUser.getId(),
              name: 'Dev Name',
              price: 100,
          } as CreateDeveloperDto;
          usersService.getById = jest.fn().mockImplementation(async id => {
              expect(id).toBe(expectedUser.getId());
              return expectedUser;
          });
          emitter.on(DeveloperCreatedEvent.event, async (e: DeveloperCreatedEvent) => {
              expect(e.getDeveloper().getPrice()).toBe(dto.price);
              expect(e.getDeveloper().getPerformance()).toBe(dto.performance);
          })
          developersRepository.save = jest.fn();
          const developer = await service.create(dto);
          expect(developersRepository.save).toHaveBeenCalled();
          expect(usersService.getById).toHaveBeenCalled();
          expect(developer.getId()).toBeDefined();
          expect(developer.getOwner()).toEqual(expectedUser);
          expect(developer.getPerformance()).toBe(dto.performance);
          expect(developer.getAvatar()).toBe(dto.avatar);
          expect(developer.getName()).toBe(dto.name);
          expect(developer.getPrice()).toBe(dto.price);
      });
  });

  describe("findByOwnerId", () => {
      it("should be returns developers by owner id", async () => {
          const ownerId = '1223-22222';
          const expectedDevelopers = [
              new Developer("113", "123", "123",100, 999, new User("ddaw", "dadw")),
              new Developer("113", "123", "123",200, 888, new User("ddaw", "dadw"))
          ];
          developersRepository.findByOwnerId = jest.fn().mockImplementation(async id => {
              expect(id).toBe(ownerId);
              return expectedDevelopers;
          });
          const developers = await service.getByOwnerId(ownerId);
          expect(developers).toEqual(expectedDevelopers);
      });
  });

  describe("find", () => {
      it("should be return founded developers", async () => {
          const expectedDevelopers = [
              new Developer("113", "123", "123",100, 209, new User("ddaw", "dadw")),
              new Developer("113", "123", "123",200, 111, new User("ddaw", "dadw"))
          ];
          developersRepository.find = jest.fn().mockImplementation(async () => expectedDevelopers);
          const developers = await service.find();
          expect(developers).toEqual(expectedDevelopers);
      });
  });

  describe("remove", () => {
      it("should be remove return developer", async () => {
          const developerId = '123444';
          const expectedDeveloper = new Developer(
              "113",
              "123",
              "123",
              100,
              304,
              new User("ddaw", "dadw")
          );
          developersRepository.getById = jest.fn().mockImplementation(async id => {
              expect(id).toBe(developerId);
              return expectedDeveloper;
          });
          developersRepository.remove = jest.fn().mockImplementation(async entity => {
              expect(entity).toEqual(expectedDeveloper);
          });
          await service.remove(developerId);
          expect(developersRepository.getById).toHaveBeenCalled();
          expect(developersRepository.remove).toHaveBeenCalled();
      });
  })
});
