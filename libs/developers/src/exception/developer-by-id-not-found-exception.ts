export class DeveloperByIdNotFoundException extends Error {
    constructor(
        public readonly id: string
    ) {
        super(`Developer with id ${id} not found`);
    }
}
