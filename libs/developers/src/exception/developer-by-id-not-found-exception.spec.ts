import { DeveloperByIdNotFoundException } from './developer-by-id-not-found-exception';

describe('DeveloperByIdNotFoundException', () => {
  it('should be defined', () => {
    expect(new DeveloperByIdNotFoundException('123')).toBeDefined();
  });
});
