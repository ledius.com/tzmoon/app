import { Test, TestingModule } from '@nestjs/testing';
import { SignupService } from './signup.service';
import { SignupUserDto } from "@app/signup/dto/signup-user-dto";
import { User, UsersService } from "@app/users";
import { Password, PasswordsService } from "@app/passwords";
import { EventEmitter2 } from "@nestjs/event-emitter";
import { SignupUserEvent } from "@app/signup/event/signup-user-event";

describe('SignupService', () => {
  let service: SignupService;
  let usersService: UsersService;
  let passwordsService: PasswordsService;
  let emitter: EventEmitter2;

  beforeEach(async () => {
    usersService = {} as UsersService;
    passwordsService = {} as PasswordsService;
    emitter = new EventEmitter2();
    const module: TestingModule = await Test.createTestingModule({
      providers: [
          SignupService,
          {
            provide: UsersService,
            useValue: usersService,
          },
          {
            provide: PasswordsService,
            useValue: passwordsService
          },
          {
            provide: EventEmitter2,
            useValue: emitter
          }
      ],
    }).compile();

    service = module.get<SignupService>(SignupService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe("signup", () => {
    let dto: SignupUserDto;
    let expectedUser: User;
    let expectedPassword: Password;

    beforeEach(() => {
      dto = {
        login: "1234",
        password: "123444"
      }
      expectedUser = new User("id", dto.login);
      expectedPassword = new Password("dawd", dto.password, expectedUser);
      usersService.create = jest.fn().mockImplementation(() => expectedUser);
      passwordsService.create = jest.fn().mockImplementation(() => expectedPassword);
    });

    it("should be controller.signup user", async () => {
      const signupUserModel = await service.signup(dto);
      expect(signupUserModel.getUser().getLogin()).toBe(dto.login);
      expect(signupUserModel.getPassword().compare(dto.password)).toBeTruthy();
      expect(signupUserModel.getPassword()).toEqual(expectedPassword);
      expect(signupUserModel.getUser()).toEqual(expectedUser);
    });
    it("should be emit controller.signup event", async () => {
      emitter.on(SignupUserEvent.event, (payload: SignupUserEvent) => {
        expect(payload).toBeInstanceOf(SignupUserEvent);
        expect(payload.user).toEqual(expectedUser);
        expect(payload.password).toEqual(expectedPassword);
      });
      await service.signup(dto);
    });
  });
});
