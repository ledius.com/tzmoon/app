import { SignupUserModel } from './signup-user-model';
import { Password } from "@app/passwords";
import { User } from "@app/users";

describe('SignupUserModel', () => {
  it('should be defined', () => {
    expect(new SignupUserModel({} as User, {} as Password)).toBeDefined();
  });

  describe("instance", () => {
    it("should be create correct instance", () => {
      const user = {test: 123} as unknown as User;
      const password = {same: 123} as unknown as Password;
      const model = new SignupUserModel(user, password);
      expect(model.getUser()).toEqual(user);
      expect(model.getPassword()).toEqual(password);
    });
  });
});
