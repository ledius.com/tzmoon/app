import { Module } from '@nestjs/common';
import { SignupService } from './service/signup.service';
import { UsersModule } from "@app/users";
import { PasswordsModule } from "@app/passwords";

@Module({
  imports: [
      UsersModule,
      PasswordsModule,
  ],
  providers: [SignupService],
  exports: [
      SignupService,
      UsersModule,
      PasswordsModule,
  ],
})
export class SignupModule {}
